//create EventListner for Button

// window.onbeforeunload = function() { return "Your work will be lost.";};

submitBtn.addEventListener('click', function(){

	//Binding the value from user input
	event.preventDefault()
	const nameOfUser = name.value;
	const typeOfBusinessValue = business.value;
	const typeOfStatusValue = status.value;
	const numberOfEmployeesValue = parseFloat(employees.value);
	const establishSitesValue = parseFloat(sites.value);
	const surveyForm = formContainer.parentElement.parentElement;


	//Functions
	
	inputValidation();

	
	
	function addUser(){

			const user = {
			id: userId,
			name: nameOfUser,
			typeOfBusiness: typeOfBusinessValue,
			typeOfStatus: typeOfStatusValue,
			numberOfEmployees: numberOfEmployeesValue,
			establishSites: establishSitesValue

		}

			userStoredInformation.push(user);
			userId++;	
		
	}


	//Login Form

	function showRegisterPage(){

		surveyForm.classList.remove('d-flex');
		surveyForm.classList.add('d-none');

		registerForm.classList.remove('d-none');
		registerForm.classList.add('d-flex');

	}
	//Added Function to insert proposal to the existing user
	function addBusinessUnderECQ(){
		if(typeOfStatusValue === "ECQ" && nameOfUser !== ""){

			const businessUnderECQ = proposal[12].id;
			const proposalTypeSelected = proposal[12].type;

			for (let index = 0; index < userStoredInformation.length; index++){
			userStoredInformation[index].retailSelected = businessUnderECQ;
			userStoredInformation[index].proposalSelected = proposalTypeSelected;

			}
		}
	}

	function addBigBusiness(){
		if(typeOfStatus !== "ECQ" && establishSitesValue >=120){
			const potentialBusinessPartner = proposal[13].id;
				const proposalTypeSelected = proposal[13].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = potentialBusinessPartner;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}

		}
	}
	//Added the function to insert proposal to the existing user
	function addRetailFoodProposal(){

		//Insert data in userStored Information and validattion
		if (typeOfBusinessValue === "Retail Food" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				const retailFoodSmallGCQ = proposal[0].id;
				const proposalTypeSelected = proposal[0].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = retailFoodSmallGCQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}
		}
		else if(typeOfBusinessValue === "Retail Food" &&  typeOfStatusValue === "GCQ" && numberOfEmployeesValue >=21){
				const retailFoodMediumGCQ = proposal[1].id;
				const proposalTypeSelected = proposal[1].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = retailFoodMediumGCQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}

		}
		else if (typeOfBusinessValue === "Retail Food" &&  typeOfStatusValue === "MECQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				const retailFoodSmallMECQ = proposal[2].id;
				const proposalTypeSelected = proposal[2].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = retailFoodSmallMECQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}
		}
		else if(typeOfBusinessValue === "Retail Food" &&  typeOfStatusValue === "MECQ" && numberOfEmployeesValue >=21){
				const retailFoodMediumMECQ = proposal[3].id;
				const proposalTypeSelected = proposal[3].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = retailFoodMediumMECQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}
			}

		}
	//Added the function to insert proposal to the existing user
	function addRetailNonFoodProposal(){

		if(typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				const retailNonFoodSmallGCQ = proposal[4].id;
				const proposalTypeSelected = proposal[4].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = retailNonFoodSmallGCQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;
			}
		}
		else if(typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue >=21){
				const retailNonFoodMediumGCQ = proposal[5].id;
				const proposalTypeSelected = proposal[5].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = retailNonFoodMediumGCQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}
		}
		else if(typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				const retailNonFoodSmallMECQ = proposal[6].id;
				const proposalTypeSelected = proposal[6].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = retailNonFoodSmallMECQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}
		}
		else if(typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue >=21){
				const retailNonFoodMediumMECQ = proposal[7].id;
				const proposalTypeSelected = proposal[7].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = retailNonFoodMediumMECQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;
				}
		}
	}

	function addOfficesProposal(){

			if(typeOfBusinessValue === "Offices" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				
				const OfficeSmallGCQ = proposal[8].id;
				const proposalTypeSelected = proposal[8].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = OfficeSmallGCQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;
				}
			}
			else if(typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue >=21){
				
				const OfficeMediumGCQ = proposal[9].id;
				const proposalTypeSelected = proposal[9].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = OfficeMediumGCQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}
			}
			else if(typeOfBusinessValue === "Offices" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){

				const OfficeSmallMECQ = proposal[10].id;
				const proposalTypeSelected = proposal[10].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = OfficeSmallMECQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}
			}
			else if(typeOfBusinessValue === "Offices" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue >=21){

				const OfficeMediumMECQ = proposal[11].id;
				const proposalTypeSelected = proposal[11].type;

				for (let index = 0; index < userStoredInformation.length; index++){
				userStoredInformation[index].retailSelected = OfficeMediumMECQ;
				userStoredInformation[index].proposalSelected = proposalTypeSelected;

				}
			}
		}


	function inputValidation(){

		//capture the error element where i'll put the error
		const error = business.parentElement.firstElementChild;

		error.innerHTML = "";

			if (nameOfUser === ""){
				
				//added the error message in the element
				error.innerHTML = `<span class="text-danger">Please insert your name.</span>`;
			}
			else if(typeOfStatusValue === "ECQ" && nameOfUser !== ""){
				
				// alert(`The data will be saved, check reference id ${userId} when get you back and when not anymore ECQ`);

				addUser();
				console.log(userStoredInformation);
				addBusinessUnderECQ();
				showRegisterPage();
			}

			else if (nameOfUser !== ""  && typeOfStatus !== "ECQ" && establishSitesValue >=120){
				addUser();
				console.log(userStoredInformation);
				addBigBusiness();
				showRegisterPage();
			}

			else if(nameOfUser !== "" && typeOfBusinessValue === "Retail Food" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				addUser();
				console.log(userStoredInformation);
				addRetailFoodProposal();
				showRegisterPage();

			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Retail Food" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue >=21){
				addUser();
				console.log(userStoredInformation);
				addRetailFoodProposal();
				showRegisterPage();
			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Retail Food" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				addUser();
				console.log(userStoredInformation);
				addRetailFoodProposal();
				showRegisterPage();
			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Retail Food" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue >=21){
				addUser();
				console.log(userStoredInformation);
				addRetailFoodProposal();
				showRegisterPage();
			}

			// Retail Non Food conditions

			else if(nameOfUser !== "" && typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				addUser();
				console.log(userStoredInformation);
				addRetailNonFoodProposal();
				showRegisterPage();


			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue >=21){
				addUser();
				console.log(userStoredInformation);
				addRetailNonFoodProposal();
				showRegisterPage();
			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				addUser();
				console.log(userStoredInformation);
				addRetailNonFoodProposal();
				showRegisterPage();
			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Retail - Non Food" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue >=21){
				addUser();
				console.log(userStoredInformation);
				addRetailNonFoodProposal();
				showRegisterPage();
			}

			// Offices condition

			else if(nameOfUser !== "" && typeOfBusinessValue === "Offices" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				addUser();
				console.log(userStoredInformation);
				addOfficesProposal();
				showRegisterPage();
			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Offices" && typeOfStatusValue === "GCQ" && numberOfEmployeesValue >=21){
				addUser();
				console.log(userStoredInformation);
				addOfficesProposal();
				showRegisterPage();
			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Offices" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue > 0 && numberOfEmployeesValue <=20){
				addUser();
				console.log(userStoredInformation);
				addOfficesProposal();
				showRegisterPage();
			}
			else if(nameOfUser !== "" && typeOfBusinessValue === "Offices" && typeOfStatusValue === "MECQ" && numberOfEmployeesValue >=21){
				addUser();
				console.log(userStoredInformation);
				addOfficesProposal();
				showRegisterPage();
			}

			else {
				return "Invalid Data";
			}

	}


})

