
loginBtn.addEventListener('click', function(){

	const loginEmailValue = loginEmail.value;
	const loginPasswordValue = loginPassword.value;

	let error = loginEmail.previousElementSibling;

	error.innerHTML = "";

	if (loginEmailValue === "" || loginPasswordValue ===""){

		error.innerHTML = `<span class="text-danger">*Please insert email or password</span>`;
		return;
	} else if (loginEmailValue != userStoredInformation.email && loginPasswordValue != userStoredInformation.password){

		error.innerHTML = `<span class="text-danger">*Incorrect email or password</span>`;

	} else if(loginEmailValue == userStoredInformation.email && loginPasswordValue == userStoredInformation.password) {
		postSelectedRetailed();
	}
	else {
		error.innerHTML = `<span class="text-danger">*Incorrect email or password</span>`;
	}
	

	function postSelectedRetailed (){
		for(let index = 0; index < userStoredInformation.length; index++){

		
		switch(userStoredInformation[index].retailSelected){

			case 1:
			window.open('Proposals/RetailFoodSmallBusinessGCQ.html', "_self");
			break;
			case 2:
			window.open('Proposals/RetailFoodMediumBusinessGCQ.html', "_self");
			break;
			case 3:
			window.open('Proposals/RetailFoodSmallBusinessMECQ.html', "_self");
			break;
			case 4:
			window.open('Proposals/RetailFoodMediumBusinessMECQ.html', "_self");
			break;
			case 5:
			window.open('Proposals/RetailNonFoodSmallBusinessGCQ.html',"_self");
			break;
			case 6:
			window.open('Proposals/RetailNonFoodMediumBusinessGCQ.html', "_self");
			break;
			case 7:
			window.open('Proposals/RetailNonFoodSmallBusinessMECQ.html', "_self");
			break;
			case 8:
			window.open('Proposals/RetailNonFoodMediumBusinessMECQ.html', "_self");
			break;
			case 9:
			window.open('Proposals/OfficeSmallBusinessGCQ.html',"_self");
			break;
			case 10:
			window.open('Proposals/OfficeMediumBusinessGCQ.html', "_self");
			break;
			case 11:
			window.open('Proposals/OfficeSmallBusinessMECQ.html', "_self");
			break;
			case 12:
			window.open('Proposals/OfficeMediumBusinessMECQ.html', "_self");
			break;
			case 13:
			window.open('Proposals/allECQBusiness.html', "_self");
			break;
			case 14:
			window.document.location = './Proposals/allBigBusiness.html'
			break;
			default:
			return "Invalid data";

		}

	}

	}

	 
})